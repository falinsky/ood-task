import add from './index';

it('should add two numbers properly', () => {
  expect(add(1, 2)).toBe(3);
});