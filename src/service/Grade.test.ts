import Grade from './Grade';
import Submission from '../model/Submission';
import SampleStudents from '../sample/SampleStudents';
import Course from '../model/Course';
import Student from '../model/Student';

describe('Grade', () => {
  let gradeService: Grade;

  describe('Calculate for particular submission', () => {
    beforeEach(() => {
      gradeService = new Grade({
        getCompletedSubmissionsForStudentAndCourseForDateRange() {
          return [];
        }
      });
    });

    [
      {
        submission: {
          answers: [],
        },
        grade: 0,
      },
      {
        submission: {
          answers: [
            {isCorrect: false},
          ],
        },
        grade: 0,
      },
      {
        submission: {
          answers: [
            {isCorrect: true},
          ],
        },
        grade: 1,
      },
      {
        submission: {
          answers: [
            {isCorrect: true},
            {isCorrect: false},
            {isCorrect: true},
            {isCorrect: false},
            {isCorrect: true},
          ],
        },
        grade: 3,
      },
    ].forEach(({submission, grade}, index) => {
      it(`grade is a sum of 1 point for each correct answer for data set #${index}`, () => {
        expect(gradeService.calculateForSubmission(submission as Submission)).toBe(grade);
      });
    });
  });

  describe('Calculate total', () => {
    let student: Student,
      course: Course,
      start: Date,
      end: Date;

    beforeEach(() => {
      student = SampleStudents.JOHN_DOE;
      course = new Course('Some course', [student]);
      start = new Date();
      end = new Date();
    });

    [
      {
        submissions: [],
        total: 0
      },
      {
        submissions: [
          {
            answers: [
              {isCorrect: true},
            ],
            finalGrade: null,
          }
        ],
        total: 1,
      },
      {
        submissions: [
          {
            answers: [],
            finalGrade: 5,
          }
        ],
        total: 5,
      },
      {
        submissions: [
          {
            answers: [
              {isCorrect: true},
              {isCorrect: false},
              {isCorrect: true},
            ],
            finalGrade: null,
          },
          {
            answers: [],
            finalGrade: 2,
          }
        ],
        total: 4,
      },
    ].forEach(({submissions, total}, index) => {
      it(`Calculate total grade for data set #${index}`, () => {
        gradeService = new Grade({
          getCompletedSubmissionsForStudentAndCourseForDateRange() {
            return submissions as Submission[];
          }
        });

        expect(gradeService.calculateTotal(student, course, start, end)).toBe(total);
      });
    });
  });

});