import Submission from '../model/Submission';
import Student from '../model/Student';
import Course from '../model/Course';
import SubmissionRepositoryInterface from '../repository/SubmissionRepositoryInterface';

/**
 * Grade service allows to calculate grades for particular submissions
 */
export default class Grade {
  constructor(private readonly submissionRepository: SubmissionRepositoryInterface) {

  }

  calculateForSubmission(submission: Submission): number {
    return submission.answers
      .map(answer => answer.isCorrect ? 1 : 0)
      .reduce((a: number, b: number): number => a + b, 0);
  }

  calculateTotal(student: Student, course: Course, start: Date, end: Date): number {
    const submissions = this.submissionRepository.getCompletedSubmissionsForStudentAndCourseForDateRange(student, course, start, end);

    return submissions.map(submission => {
      if (submission.finalGrade !== null) {
        return submission.finalGrade;
      }

      return this.calculateForSubmission(submission);
    }).reduce((a, b) => a + b, 0);
  }
}