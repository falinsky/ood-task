import Question from '../model/Question';
import SampleChoices from './SampleChoices';

export default class SampleQuestions {
  static EASY = new Question(
    'Easy question',
    [SampleChoices.A, SampleChoices.B],
    SampleChoices.B
  );

  static MODERATE = new Question(
    'Moderate question',
    [SampleChoices.C, SampleChoices.D, SampleChoices.E],
    SampleChoices.D
  );

  static HARD = new Question(
    'Hard question',
    [SampleChoices.F, SampleChoices.G, SampleChoices.H, SampleChoices.I],
    SampleChoices.G
  );
}