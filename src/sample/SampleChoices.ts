import Choice from '../model/Choice';

export default class SampleChoices {
  static A = new Choice('A');
  static B = new Choice('B');
  static C = new Choice('C');
  static D = new Choice('D');
  static E = new Choice('E');
  static F = new Choice('F');
  static G = new Choice('G');
  static H = new Choice('H');
  static I = new Choice('I');
}