import Student from '../model/Student';

export default class SampleStudents {
  static JOHN_DOE = new Student('John', 'Doe');
  static JANE_DOE = new Student('Jane', 'Doe');
  static RICHARD_MILES = new Student('Richard', 'Miles');
}