import Quiz from '../model/Quiz';
import SampleQuestions from './SampleQuestions';

export default class SampleQuizzes {
  static STANDARD = new Quiz(
    'Standard quiz',
    [
      SampleQuestions.EASY,
      SampleQuestions.MODERATE
    ]
  );

  static DIFFICULT = new Quiz(
    'Difficult quiz',
    [
      SampleQuestions.HARD
    ]
  );
}