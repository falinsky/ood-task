/**
 * Represents one choice for a multiple choice question
 */
export default class Choice {
  constructor(readonly text: string) {
    if (text.length === 0) {
      throw new Error('Text cannot be empty');
    }
  }
}