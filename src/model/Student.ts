/**
 * Represents a student.
 */
export default class Student {
  constructor(readonly firstName: string, readonly lastName: string) {
    if (firstName.length === 0) {
      throw new Error('Fist name cannot be empty');
    }

    if (lastName.length === 0) {
      throw new Error('Last name cannot be empty');
    }
  }
}