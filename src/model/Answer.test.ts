import SampleQuestions from '../sample/SampleQuestions';
import Answer from './Answer';
import Choice from './Choice';

describe('Answer', () => {
  it('can be created', () => {
    const question = SampleQuestions.EASY;
    const choice = SampleQuestions.EASY.allChoices[0];

    const answer = new Answer(question, choice);

    expect(answer.question).toBe(question);
    expect(answer.choice).toBe(choice);
  });

  it('should not accept unknown choice', () => {
    expect(() => new Answer(SampleQuestions.EASY, new Choice('Unknown choice'))).toThrow()
  });

  it('should be identified as correct', () => {
    const answer = new Answer(SampleQuestions.EASY, SampleQuestions.EASY.allChoices[1]);

    expect(answer.isCorrect).toBeTruthy();
  });

  it('should be identified as not correct', () => {
    const answer = new Answer(SampleQuestions.EASY, SampleQuestions.EASY.allChoices[0]);

    expect(answer.isCorrect).toBeFalsy();
  });
});