import Quiz from './Quiz';
import SampleQuestions from '../sample/SampleQuestions';

describe('Quiz', () => {
  it('can be created', () => {
    const name = 'Quiz Name';
    const questions = [SampleQuestions.EASY, SampleQuestions.HARD];

    const quiz = new Quiz(name, questions);

    expect(quiz.name).toBe(name);
    questions.forEach(question => {
      expect(quiz.containsQuestion(question)).toBeTruthy();
    });
    expect(quiz.questions.length).toBe(questions.length);
  });

  it('cannot have empty text', () => {
    expect(() => new Quiz('', [SampleQuestions.MODERATE])).toThrow();
  });

  it('cannot have less than one question', () => {
    expect(() => new Quiz('Quiz with 0 questions', [])).toThrow();
  });
});