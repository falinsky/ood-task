import Choice from './Choice';

/**
 * Represents a multiple choice question. Supports only one correct choice.
 */
export default class Question {
  private _allChoices: Choice[];

  constructor(readonly text: string, choices: Choice[], readonly correctChoice: Choice) {
    if (text.length === 0) {
      throw new Error('Text cannot be empty');
    }

    if (choices.length < 2) {
      throw new Error('There should be at least 2 choices provided');
    }

    this._allChoices = choices;

    if (!this.containsChoice(correctChoice)) {
      throw new Error('Correct choice is unknown');
    }
  }

  get allChoices(): Choice[] {
    return [...this._allChoices];
  }

  containsChoice(choice: Choice): boolean {
    return this._allChoices.indexOf(choice) !== -1;
  }
}