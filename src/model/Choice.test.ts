import Choice from './Choice';

describe('Choice', () => {
  it('can be created', () => {
    const text = 'Simple choice';
    const choice = new Choice(text);

    expect(choice.text).toBe(text);
  });

  it('cannot have empty text', () => {
    expect(() => new Choice('')).toThrow();
  });
});