import Student from './Student';

describe('Student', () => {
  it('can be created', () => {
    const firstName = 'FirstName';
    const lastName = 'LastName';

    const student = new Student(firstName, lastName);

    expect(student.firstName).toBe(firstName);
    expect(student.lastName).toBe(lastName);
  });

  it('cannot have an empty first name', () => {
    expect(() => new Student('', 'NotEmpty')).toThrow();
  });

  it('cannot have an empty last name', () => {
    expect(() => new Student('NotEmpty', '')).toThrow();
  });
});