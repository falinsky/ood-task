import Submission from './Submission';
import SampleStudents from '../sample/SampleStudents';
import Course from './Course';
import SampleQuizzes from '../sample/SampleQuizzes';
import Student from './Student';
import Quiz from './Quiz';
import Answer from './Answer';

describe('Submission', () => {
  let student: Student,
    quiz: Quiz,
    course: Course,
    submission: Submission;

  beforeEach(() => {
    student = SampleStudents.JOHN_DOE;
    quiz = SampleQuizzes.STANDARD;
    course = new Course(
      'Course with standard quiz and John Doe student',
      [student]
    );
    course.addQuiz(quiz);
  });

  it('can be created', () => {
    submission = new Submission(course, quiz, student);

    expect(submission.course).toBe(course);
    expect(submission.quiz).toBe(quiz);
    expect(submission.student).toBe(student);
    expect(submission.createdAt).toBeInstanceOf(Date);
  });

  it('cannot have a quiz for a class not aware of it', () => {
    const quiz = SampleQuizzes.DIFFICULT;

    expect(() => new Submission(course, quiz, student)).toThrow();
  });

  it('cannot have a student for a class not aware of him', () => {
    const student = SampleStudents.JANE_DOE;

    expect(() => new Submission(course, quiz, student)).toThrow();
  });

  describe('Answers', () => {
    beforeEach(() => {
      submission = new Submission(course, quiz, student);
    });

    it('should be empty by default', () => {
      expect(submission.answers.length).toBe(0);
    });

    it('should add answers to known questions', () => {
      const answers = quiz.questions.map(question => {
        return new Answer(question, question.correctChoice);
      });

      answers.forEach(answer => {
        submission.addAnswer(answer);
      });

      answers.forEach(answer => {
        expect(submission.containsAnswer(answer)).toBe(true);
      });

      expect(submission.answers.length).toBe(quiz.questions.length);
    });

    it('should not add answer to unknown question', () => {
      const unknownQuestion = SampleQuizzes.DIFFICULT.questions[0];
      const answer = new Answer(unknownQuestion, unknownQuestion.correctChoice);

      expect(() => submission.addAnswer(answer)).toThrow();
    });

    it('should not add the same answer multiple times', () => {
      const knownQuestion = quiz.questions[0];
      const answer = new Answer(knownQuestion, knownQuestion.correctChoice);

      submission.addAnswer(answer);

      expect(() => submission.addAnswer(answer)).toThrow();
    });
  });

  describe('Completion', () => {
    beforeEach(() => {
      submission = new Submission(course, quiz, student);
    });

    it('should not be completed by default', () => {
      expect(submission.completed).toBe(false);
      expect(submission.completedAt).toBeNull();
    });

    it('should be completed after adding answers and call complete method', () => {
      quiz.questions.map(question => {
        submission.addAnswer(new Answer(question, question.correctChoice));
      });

      submission.complete();

      expect(submission.completed).toBe(true);
      expect(submission.completedAt).not.toBeNull();
    });

    it('should not be completed if no answers provided', () => {
      expect(() => submission.complete()).toThrow();
    });

    it('should not be completed if not all answers provided', () => {
      const question = quiz.questions[0];
      submission.addAnswer(new Answer(question, question.correctChoice));

      expect(() => submission.complete()).toThrow();
    });
  });

  describe('Final grade', () => {
    beforeEach(() => {
      submission = new Submission(course, quiz, student);
    });

    it('should not be provided by default', () => {
      expect(submission.finalGrade).toBeNull();
    });

    describe('complete submission', () => {
      beforeEach(() => {
        quiz.questions.map(question => {
          submission.addAnswer(new Answer(question, question.correctChoice));
        });
        submission.complete();
      });

      it('final grade can be provided after completing the submission', () => {
        const finalGrade = 100;
        submission.evaluate(finalGrade);

        expect(submission.finalGrade).toBe(finalGrade);
      });

      it('should not accept grade less than 0', () => {
        expect(() => submission.evaluate(-1)).toThrow();
      })
    });

    it('should not be accepted for not completed submissions', () => {
      expect(submission.completed).toBe(false);
      expect(() => submission.evaluate(100)).toThrow();
    });
  });
});