import Teacher from './Teacher';
import Course from './Course';

describe('Teacher', () => {
  it('can be created', () => {
    const firstName = 'FirstName';
    const lastName = 'LastName';

    const teacher = new Teacher(firstName, lastName);

    expect(teacher.firstName).toBe(firstName);
    expect(teacher.lastName).toBe(lastName);
  });

  it('cannot have an empty first name', () => {
    expect(() => new Teacher('', 'NotEmpty')).toThrow();
  });

  it('cannot have an empty last name', () => {
    expect(() => new Teacher('NotEmpty', '')).toThrow();
  });

  describe('Courses', () => {
    let teacher: Teacher;

    beforeEach(() => {
      teacher = new Teacher('Name', 'Surname');
    });

    it('should be empty by default', () => {
      expect(teacher.courses.length).toBe(0);
    });

    it('can be added', () => {
      const courses = [new Course('First course'), new Course('Second course')];
      courses.forEach(course => {
        teacher.addCourse(course);
      });

      courses.forEach(course => {
        expect(teacher.containsCourse(course)).toBe(true);
      });

      expect(teacher.courses.length).toBe(courses.length);
    });

    it('cannot add the same course multiple times', () => {
      const course = new Course('Simple course');
      teacher.addCourse(course);

      expect(() => teacher.addCourse(course)).toThrow();
    });
  });
});