import Question from './Question';
import Choice from './Choice';

/**
 * Holds student's answer (particular choice) to particular question
 */
export default class Answer {
  constructor(readonly question: Question, readonly choice: Choice) {
    if (!question.containsChoice(choice)) {
      throw new Error('Unknown choice provided');
    }
  }

  get isCorrect(): boolean {
    return this.question.correctChoice === this.choice;
  }
}