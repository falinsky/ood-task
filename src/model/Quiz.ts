import Question from './Question';

/**
 * Represents a quiz. Can contain many questions.
 */
export default class Quiz {
  private _questions: Question[];

  constructor(readonly name: string, questions: Question[]) {
    if (name.length === 0) {
      throw new Error('Name cannot be empty');
    }

    if (questions.length === 0) {
      throw new Error('Questions cannot be empty');
    }

    this._questions = questions;
  }

  get questions(): Question[] {
    return [...this._questions];
  }

  containsQuestion(question: Question): boolean {
    return this._questions.indexOf(question) !== -1;
  }
}