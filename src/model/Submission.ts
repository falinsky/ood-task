import Course from './Course';
import Quiz from './Quiz';
import Student from './Student';
import Answer from './Answer';

/**
 * Represents a bunch of answers related to questions from a quiz
 * from particular course for particular student. Can be graded.
 */
export default class Submission {
  readonly createdAt: Date = new Date();
  private _completedAt: Date | null = null;
  private _finalGrade: number | null = null;
  private _answers: Answer[] = [];

  constructor(readonly course: Course, readonly quiz: Quiz, readonly student: Student) {
    if (!course.containsQuiz(quiz)) {
      throw new Error('Provided course is not aware of provided quiz');
    }

    if (!course.containsStudent(student)) {
      throw new Error('Provided course is not aware of provided student');
    }
  }

  get completedAt(): Date | null {
    return this._completedAt;
  }

  get completed(): boolean {
    return this.completedAt !== null;
  }

  get finalGrade(): number | null {
    return this._finalGrade;
  }

  get answers(): Answer[] {
    return [...this._answers];
  }

  addAnswer(answer: Answer): void {
    if (!this.quiz.containsQuestion(answer.question)) {
      throw new Error('Answer provided to unknown question');
    }

    if (this.containsAnswer(answer)) {
      throw new Error('Adding the same answer multiple times is not allowed');
    }

    this._answers.push(answer);
  }

  containsAnswer(answer: Answer): boolean {
    return this._answers.indexOf(answer) !== -1;
  }

  complete(): void {
    const answerQuestions = this._answers.map(answer => answer.question);

    if (this.quiz.questions.every(question => answerQuestions.indexOf(question) !== 1)) {
      throw new Error('Answers must be provided to all questions');
    }

    this._completedAt = new Date();
  }

  evaluate(grade: number): void {
    if (grade < 0) {
      throw new Error('Grades less than 0 are not allowed');
    }

    if (!this.completed) {
      throw new Error('Evaluation of not completed submission is not allowed');
    }

    this._finalGrade = grade;
  }
}