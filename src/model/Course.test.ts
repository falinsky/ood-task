import Course from './Course';
import SampleStudents from '../sample/SampleStudents';
import SampleQuizzes from '../sample/SampleQuizzes';

describe('Course', () => {
  it('can be created', () => {
    const name = 'Course Name';

    const course = new Course(name);

    expect(course.name).toBe(name);
  });

  it('cannot have an empty name', () => {
    expect(() => new Course('')).toThrow();
  });

  it('does not have students by default', () => {
    const emptyCourse = new Course('Without students');

    expect(emptyCourse.students.length).toBe(0);
  });

  it('should populate students when provided during creation', () => {
    const students = [
      SampleStudents.JOHN_DOE,
      SampleStudents.JANE_DOE,
      SampleStudents.RICHARD_MILES
    ];

    const courseWithStudents = new Course('Course with students', students);

    students.forEach(student => {
      expect(courseWithStudents.containsStudent(student)).toBeTruthy();
    });

    expect(courseWithStudents.students.length).toBe(students.length);
  });

  it('does not have quizzes by default', () => {
    const emptyCourse = new Course('Without quizzes');

    expect(emptyCourse.quizzes.length).toBe(0);
  });

  it('can add quizzes', () => {
    const quizCourse = new Course('For quizzes');
    const quizzes = [SampleQuizzes.STANDARD, SampleQuizzes.DIFFICULT];

    quizzes.forEach(quiz => {
      quizCourse.addQuiz(quiz);

      expect(quizCourse.containsQuiz(quiz)).toBeTruthy();
    });

    expect(quizCourse.quizzes.length).toBe(quizzes.length);
  });

  it('cannot add the same quiz multiple times', () => {
    const quizCourse = new Course('For quizzes');
    const quiz = SampleQuizzes.STANDARD;

    quizCourse.addQuiz(quiz);

    expect(() => quizCourse.addQuiz(quiz)).toThrow();
  });
});