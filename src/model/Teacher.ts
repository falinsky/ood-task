import Course from './Course';

/**
 * Represents a teacher.
 * Courses can be assigned to a teacher.
 */
export default class Teacher {
  private _courses: Course[] = [];

  constructor(readonly firstName: string, readonly lastName: string) {
    if (firstName.length === 0) {
      throw new Error('Fist name cannot be empty');
    }

    if (lastName.length === 0) {
      throw new Error('Last name cannot be empty');
    }
  }

  get courses(): Course[] {
    return [...this._courses];
  }

  addCourse(course: Course): void {
    if (this.containsCourse(course)) {
      throw new Error('Adding the same course multiple times is not allowed');
    }

    this._courses.push(course);
  }

  containsCourse(course: Course): boolean {
    return this._courses.indexOf(course) !== -1;
  }
}