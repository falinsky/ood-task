import Student from './Student';
import Quiz from './Quiz';

/**
 * Represents a course which contains students.
 * Quizzes can be assigned to a course.
 */
export default class Course {
  private _students: Student[];
  private _quizzes: Quiz[] = [];

  constructor(readonly name: string, students: Student[] = []) {
    if (name.length === 0) {
      throw new Error('Name cannot be empty');
    }

    this._students = students;
  }

  get students(): Student[]{
    return [...this._students];
  }

  get quizzes(): Quiz[] {
    return [...this._quizzes];
  }

  containsQuiz(quiz: Quiz): boolean {
    return this._quizzes.indexOf(quiz) !== -1;
  }

  containsStudent(student: Student): boolean {
    return this._students.indexOf(student) !== -1;
  }

  addQuiz(quiz: Quiz): void {
    if (this.containsQuiz(quiz)) {
      throw new Error('Adding the same quiz multiple times is not allowed');
    }
    this._quizzes.push(quiz);
  }
}