import Choice from './Choice';
import Question from './Question';

describe('Question', () => {
  it('can be created', () => {
    const text = 'Question text';
    const choices = [
      new Choice('First'),
      new Choice('Second'),
      new Choice('Third'),
    ];
    const correctChoice = choices[0];

    const question = new Question(text, choices, correctChoice);

    expect(question.text).toBe(text);
    choices.forEach(choice => {
      expect(question.containsChoice(choice)).toBeTruthy();
    });
    expect(question.allChoices.length).toBe(choices.length);
    expect(question.correctChoice).toBe(correctChoice);
  });

  it('cannot have empty text', () => {
    const choices = [new Choice('First'), new Choice('Second')];

    expect(() => new Question('', choices, choices[0])).toThrow();
  });

  it('cannot have less than two choices', () => {
    const choices = [new Choice('First')];

    expect(() => new Question('Question with empty choices', [], choices[0])).toThrow();
    expect(() => new Question('Question with one choice', choices, choices[0])).toThrow();
  });

  it('should have known correct choice', () => {
    const choices = [new Choice('First'), new Choice('Second')];

    expect(() => new Question('Question with unknown correct choice', choices, new Choice('Third'))).toThrow();
  });
});