import Student from '../model/Student';
import Course from '../model/Course';
import Submission from '../model/Submission';

/**
 * Real implementations of this interface should be used to retrieve students submissions
 * from some place (such as a database).
 */
export default interface SubmissionRepositoryInterface {
  getCompletedSubmissionsForStudentAndCourseForDateRange(
    student: Student,
    course: Course,
    start: Date,
    end: Date
  ): Submission[];
}